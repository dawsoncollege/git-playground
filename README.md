# Git Playground

This repo is intended for experimenting with git commands
and concepts. Feel free to add branches, create Merge Requests (but don't merge them).

Use this repo in our [git tutorials](https://gitlab.com/dawsoncollege/git-tutorials/-/blob/main/README.md?ref_type=heads)
